import './App.css';
import StatewiseData from './StatewiseData';
import Header from './Header'

function App() {
  return (
    <div>
      <Header className="center"/>
      <StatewiseData/>
    </div>
  );
}

export default App;
