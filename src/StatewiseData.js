import React from "react";
import { useEffect, useState } from "react";
import Data from "./Data";
import "./StatewiseData.css";

const StatewiseData = () => {
	const getCovidData = async () => {
		const res = await fetch("https://api.covid19india.org/data.json");
		const actualData = await res.json();
		console.log(actualData.statewise);
	};

	const [data, setData] = useState([]);
	const [search, setsearch] = useState("");

	useEffect(() => {
		getCovidData();
	}, []);

	// const filteredState = state.filter((state) =>
	// 	state.name.toLowerCase().includes(search.toLowerCase())
	// );

	const handleChange = (e) => {
		setsearch(e.target.value);
	};

	return (
		<>
			<div>
				<div className="coin-search">
					<h1 className="coin-text">Search State</h1>
					<form>
						<input
							type="text"
							placeholder="Search"
							className="coin-input"
							onChange={handleChange}
						/>
					</form>
				</div>
				{data.map((currElem, ind) => {
					return (
						<Data
							key={currElem.state}
							// name={data.name}
							// image={data.image}
							// symbol={data.symbol}
							// marketcap={data.market_cap}
							// price={data.current_price}
							// priceChange={data.price_change_percentage_24h}
							// volume={data.total_volume}
						/>
					);
				})}
			</div>
		</>
	);
};

export default StatewiseData;
