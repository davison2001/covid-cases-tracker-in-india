import React from 'react'
import "./Data.css"

const Data = (state, confirmed, recovered, deaths, active, lastupdated) => {
    return (
        <div className="coin-container">
            <div className="coin-row">
                <div className="coin">
                    {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                    <h1>{state}</h1>
                    
                </div>
                <div className="coin-data">
                    <p className="coin-price">{confirmed}</p>
                    <p className="coin-volume green">{recovered}</p>
                    
                        <p className="coin-percent red">{deaths}</p>
                    {/* (<p className="coin-percent green">{priceChange.toFixed(2)}%</p>) */}
                    
                    <p className="coin-marketcap">
                        {active}
                    </p>
                    <p className="coin-marketcap">
                        {lastupdated}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Data
